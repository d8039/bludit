#!/bin/sh

echo "[+] Starting services ..."

echo "  - php-fpm ..."
php-fpm83 --daemonize

echo "  - nginx ..."
nginx -g "daemon off;"
