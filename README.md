# danielsan05/bludit

## Description

Bludit flat file CMS Docker Hub image based on latest Alpine Linux, nginx as Reverse Proxy within the container and current PHP 8.3 version.
This Docker image will be updated every two weeks based on the current and latest Alpine Linux.
This image uses a sane nginx config from [h5bp](https://github.com/h5bp/server-configs-nginx) and some modified settings based on [Bludit Nginx config](https://docs.bludit.com/en/webservers/nginx).

This project is a fork of GitHub's [laugmanuel/bludit](https://github.com/laugmanuel/bludit-docker) project but hosted on [GitLab](https://gitlab.com/d8039/bludit), and there are further differences:

- ARM64 CPU support
- latest Alpine Linux image will be used
- PHP 8.3 version will be used
- Bludit docker image will be built every two weeks on Sunday night based on release concept of Alpine Linux lifecycle management

## Overview

- FROM: alpine:latest
- nginx
- PHP 8.3 + FPM

The image uses the nginx user with id 555.

**Please note:** The Docker container contains no configurations for SSL certificates. Deploying this Docker container behind an extra running frontend proxy container (e.g. Traefik 2 or another nginx container) is absolutely **mandatory**.

## Deploying Example
**Prerequisites:**
- Create the three folders
  - **"bl-plugins"**
  - **"bl-themes"**
  - **"bl-content"**

  on the relative directory path of your Docker Host where you want to execute the dokcer run CLI command.
Then paste one of the following snippets into the CLI of your Docker Host:
```
  docker run -p 8080:8080 \
  -v $(pwd)/bl-plugins:/usr/share/nginx/html/bl-plugins \
  -v $(pwd)/bl-themes:/usr/share/nginx/html/bl-themes \
  -v $(pwd)/bl-content:/usr/share/nginx/html/bl-content \
  -it danielsan05/bludit:latest`

# or to skip theme installation

docker run -p 8080:8080 \
  -v $(pwd)/bl-plugins:/usr/share/nginx/html/bl-plugins \
  -v $(pwd)/bl-themes:/usr/share/nginx/html/bl-themes \
  -v $(pwd)/bl-content:/usr/share/nginx/html/bl-content \
  -e SKIP_THEMES_INSTALL=true \
  -it danielsan05/bludit:latest
  ```
## Environment variables
### SKIP_THEMES_INSTALL (default: false)
Skips the installation of build-in bludit themes if set to true.

**Note:** You need to keep the themes up-to-date yourself! Furture bludit releases might not work with old themes!
### SKIP_PLUGINS_INSTALL (default: false)
Skips the installation of build-in bludit plugins if set to true.

**Note:** You need to keep the plugins up-to-date yourself! Furture bludit releases might break with old plugins!

## Example Docker Compose file
**Prerequisites:**
- Create a subfolder called **"bludit"** under your target directory where the docker-compose.yml file will be saved.
- Create the three subfolders
  - **"bl-plugins"**
  - **"bl-themes"**
  - **"bl-content"**

  under the **"bludit"** folder.

  This persists each Bludit settings, themes and plugins in an extra docker volume which have to be maintained over the Bludit admin panel as you were used to it from a direct installation on a LAMP stack.
```
version: "3.9"
services:
  bludit:
    image: danielsan05/bludit:latest
    container_name: bludit
    restart: always
    security_opt:
      - no-new-privileges=true
    depends_on:
      - proxy
    volumes:
      - './bludit/bl-plugins:/usr/share/nginx/html/bl-plugins'
      - './bludit/bl-themes:/usr/share/nginx/html/bl-themes'
      - './bludit/bl-content:/usr/share/nginx/html/bl-content'
    expose:
      - '8080'
    networks:
      proxy:
networks:
  proxy:
    name: proxy
    driver: bridge
```

**Please note:** The configuration settings for integrating in your frontend proxy container are missing and have to be added manually based on your needs and environment.

Finally run `docker-compose up -d` to start the container.
